﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public float levelStartDelay = 1.5f;
	public float toastDelay = 2f;
	public static GameManager instance = null;
	[HideInInspector] public IBoardManager boardScript;
	[HideInInspector] public GameObject playerInstance = null;

	private int level = 1;
	private int score = 0;
	private int maxLevel = 0;
	private Text levelText;
	private Text scoreText;
	private Text toastText;
	private GameObject levelImage;
	[HideInInspector] public bool doingSetup;
	public AudioClip nextLevelSound;
	public AudioClip gameOverSound;

	private bool toastScore10 = false;
	private bool toastScore20 = false;
	private bool toastScore30 = false;
	private bool toastScore40 = false;
	private bool toastLevel4 = false;
	private bool toastLevel6 = false;
	private bool toastLevel8 = false;
	private bool toastLevel10 = false;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);

		boardScript = GetComponent<IBoardManager>();
		playerInstance = GameObject.FindGameObjectWithTag("Player");
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		scoreText = GameObject.Find("Score Text").GetComponent<Text>();
		toastText = GameObject.Find("Toast Text").GetComponent<Text>();

		InitGame();
		Toast ("Hello!");
	}

	void InitGame()
	{
		doingSetup = true;
		levelText.text = "Floor " + level;
		levelImage.SetActive(true);

		if(boardScript != null)
			boardScript.SetupScene(level);

		Invoke("HideLevelImage", levelStartDelay);
	}

	private void HideLevelImage()
	{
		levelImage.SetActive(false);
		doingSetup = false;
	}

	public void GameOver()
	{
		level--;
		string tag = "GG";
		if(level < maxLevel)
			tag = "FML";
		if (level > maxLevel) {
			maxLevel = level;
			tag = "MRN";
		}

		levelText.text = "he ded :,( #RIP" + (level) + " #" + tag + maxLevel;
		levelImage.SetActive(true);
		enabled = false;
		doingSetup = true;
		SoundManager.instance.PlaySingle(gameOverSound);

		level = 1;
		score = 0;
		scoreText.text = "Start!";
		if(boardScript != null && boardScript.ResetPlayerPositionOnLevelChange())
			playerInstance.transform.position = new Vector3(0f, 0f, 0f);
		Invoke("InitGame", levelStartDelay);
	}

	public void NextLevel()
	{
		if(boardScript != null && boardScript.ResetPlayerPositionOnLevelChange())
			playerInstance.transform.position = new Vector3(0f, 0f, 0f);
		level++;
		InitGame();

		if (level == 4 && !toastLevel4) {
			toastLevel4 = true;
			Toast ("Off the Ground (Floor 4)");
		}
		if (level == 6 && !toastLevel6) {
			toastLevel6 = true;
			Toast ("Telleporter (Floor 6)");
		}
		if (level == 8 && !toastLevel8) {
			toastLevel8 = true;
			Toast ("Tellepropmter (Floor 8)");
		}
		if (level == 10 && !toastLevel10) {
			toastLevel10 = true;
			Toast ("Soaring (Floor 10)");
		}
	}

	public void MoreScore()
	{
		score++;
		scoreText.text = "Level " + score + " Demon";

		if (score == 10 && !toastScore10) {
			toastScore10 = true;
			Toast ("Appetizer (Score 10)");
		}
		if (score == 20 && !toastScore20) {
			toastScore20 = true;
			Toast ("Small Lunch (Score 20)");
		}
		if (score == 30 && !toastScore30) {
			toastScore30 = true;
			Toast ("Must be Hungry (Score 30)");
		}
		if (score == 40 && !toastScore40) {
			toastScore40 = true;
			Toast ("Full Course (Score 40)");
		}
	}

	public void Toast(string message)
	{
		toastText.text = message;
		toastText.enabled = true;
		Invoke("HideToast", toastDelay);
	}
	private void HideToast()
	{
		toastText.enabled = false;
	}
}
