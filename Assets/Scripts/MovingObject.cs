﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour
{
	public float moveTime = 0.1f;
	public float cantMoveWaitTime = 0.2f;


	private LayerMask blockingLayer;
	private BoxCollider2D boxCollider;
	private float inverseMoveTime;
	private bool isMoving = false;


	protected virtual void Start()
	{
		blockingLayer = LayerMask.GetMask("BlockingLayer");
		boxCollider = GetComponent<BoxCollider2D>();
		inverseMoveTime = 1f / moveTime;
	}

	// The author said "x deer" instead of "x dir"
	protected bool Move(int xDeer, int yDeer, out RaycastHit2D hit)
	{
		Vector2 start = transform.position;
		Vector2 end = start + new Vector2(xDeer, yDeer);

		boxCollider.enabled = false;
		hit = Physics2D.Linecast(start, end, blockingLayer);
		boxCollider.enabled = true;

		if(hit.transform == null)
		{
			StartCoroutine(SmoothMovement(end));
			return true;
		}

		return false;
	}

	// TODO do this on update instead 
	protected IEnumerator SmoothMovement(Vector3 end)
	{
		isMoving = true;

		// move the sprite to a the old position (a local delta, so something like [-1, 0])
		// move the object directly to the new position
		// the sprite will stay as an afterimage while the object is in the new position
		Transform spriteTransform = GetComponentInChildren<SpriteRenderer>().transform;
		spriteTransform.localPosition = transform.position - end;
		transform.position = end;

		// now float the sprite towards the object until it's at a [0,0] delta
		float sqrRemainingDistance = spriteTransform.localPosition.sqrMagnitude;
		while(sqrRemainingDistance > float.Epsilon)
		{
			Vector3 newPosition = Vector3.MoveTowards(spriteTransform.localPosition, Vector3.zero, inverseMoveTime * Time.deltaTime);
			spriteTransform.localPosition = newPosition;
			sqrRemainingDistance = spriteTransform.localPosition.sqrMagnitude;
			yield return null;
		}

		OnMovementFinished();
		isMoving = false;
	}

	protected virtual bool AttemptMove<T>(int xDir, int yDir)
		where T : Component
	{
		if(isMoving || (GameManager.instance != null && GameManager.instance.doingSetup)) return false;
		
		RaycastHit2D hit;
		bool canMove = Move(xDir, yDir, out hit);

		if(hit.transform == null)
			return canMove;

		T hitComponent = hit.transform.GetComponent<T>();

		if(!canMove && hitComponent != null)
		{
			OnCantMove(hitComponent);
			isMoving = true;
			Invoke("CantMoveWaitTimeFinished", cantMoveWaitTime);
		}

		return canMove;
	}

	private void CantMoveWaitTimeFinished()
	{
		isMoving = false;
	}

	protected virtual void OnMovementFinished()
	{
	}

	/** @return true if the mover should pause for a moment */
	protected abstract bool OnCantMove<T>(T component)
		where T : Component;
}
