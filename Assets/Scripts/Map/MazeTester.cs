﻿using UnityEngine;
using System.Collections;

public class MazeTester : MonoBehaviour
{
	public static MazeTester instance = null;
	[HideInInspector] private TiledBoardLayout boardScript;

	public GameObject roomTile;
	public GameObject shortRoomTile;
	public GameObject doorTile; // show a connection

	void Awake()
	{
		instance = this;
		boardScript = GetComponent<TiledBoardLayout>();

//		MazeGenerator.Room[,] maze = MazeGenerator.createMaze (7, 7);
//		MazeGenerator.FindShortestPath (maze, new Vector2 (0, 0), new Vector2 (6, 6));
//		MazeGenerator.DebugDrawRoom (maze);

		boardScript.SetupScene(1);
	}
}
