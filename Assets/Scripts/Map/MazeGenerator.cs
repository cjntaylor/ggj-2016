﻿using UnityEngine;
using Random = UnityEngine.Random;
using System.Collections;
using System.Collections.Generic;

public class MazeGenerator : MonoBehaviour {

	/* room connections */
	public class Room
	{
		public int x;
		public int y;
		public bool isOnShortestPath = false;
		public bool north = false;
		public bool south = false;
		public bool east = false;
		public bool west = false;
		public Room(int x, int y, bool n, bool s, bool e, bool w) {
			this.x = x; this.y = y;
			north = n; south = s; east = e; west = w;
		}

		public override string ToString ()
		{
			return "[" + x + "," + y + "] " +
				(north ? "N" : "_") +
				(south ? "S" : "_") +
				(east ? "E" : "_") +
				(west ? "W" : "_");
		}
	}

	public static Room[,] createMaze(int width, int height) {
		// [x0, y0] is the bottom left
		// [x1, y0] is north of [x0, y0]
		// [x0, y1] is west of [x0, y0]
		Room[,] maze = new Room[width,height];

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				maze [x, y] = new Room (x, y, false, false, false, false);

		List<Room> list = new List<Room>();
		list.Add (new Room(0, 1, false, true, false, false));
		list.Add (new Room(1, 0, false, false, false, true));
		while(list.Count > 0)
		{
			int randIdx = Random.Range (0, list.Count);
			Room r = list[randIdx];
			list.RemoveAt(randIdx);

			if (!IsRoomInMaze(maze, r))
			{
				AddRoomToMaze (maze, r);

				if(r.x > 0)
					list.Add (new Room(r.x-1, r.y, false, false, true, false));
				if(r.x < width - 1)
					list.Add (new Room(r.x+1, r.y, false, false, false, true));
				if(r.y > 0)
					list.Add (new Room(r.x, r.y-1, true, false, false, true));
				if(r.y < height - 1)
					list.Add (new Room(r.x, r.y+1, false, true, false, true));
			}
		}

		return maze;
	}

	private static bool IsRoomInMaze(Room[,] maze, Room room)
	{
		Room roomToTest = maze[room.x, room.y];

//		if (room.north)
//			roomToTest = maze[room.x, room.y+1];
//		else if(room.south)
//			roomToTest = maze[room.x, room.y-1];
//		else if(room.east)
//			roomToTest = maze[room.x+1, room.y];
//		else if(room.west)
//			roomToTest = maze[room.x-1, room.y];

		return roomToTest.north || roomToTest.south || roomToTest.east || roomToTest.west;
	}

	private static void AddRoomToMaze(Room[,] maze, Room room)
	{
		if (room.north) {
			maze [room.x, room.y + 1].south = true;
			maze [room.x, room.y].north = true;
		} else if (room.south) {
			maze [room.x, room.y - 1].north = true;
			maze [room.x, room.y].south = true;
		} else if (room.east) {
			maze [room.x + 1, room.y].west = true;
			maze [room.x, room.y].east = true;
		} else if (room.west) {
			maze [room.x - 1, room.y].east = true;
			maze [room.x, room.y].west = true;
		}
	}


	public static void DebugDrawRoom(Room[,] maze)
	{
		int width = maze.GetLength (0);
		int height = maze.GetLength (1);

		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
				if (maze [x, y].isOnShortestPath)
					Instantiate(MazeTester.instance.shortRoomTile, new Vector3(x*2, y*2, 0f), Quaternion.identity);
				else
					Instantiate(MazeTester.instance.roomTile, new Vector3(x*2, y*2, 0f), Quaternion.identity);

				if(maze[x,y].north)
					Instantiate(MazeTester.instance.doorTile, new Vector3(x*2, y*2+1, 0f), Quaternion.identity);
				if(maze[x,y].south)
					Instantiate(MazeTester.instance.doorTile, new Vector3(x*2, y*2-1, 0f), Quaternion.identity);
				if(maze[x,y].east)
					Instantiate(MazeTester.instance.doorTile, new Vector3(x*2+1, y*2, 0f), Quaternion.identity);
				if(maze[x,y].west)
					Instantiate(MazeTester.instance.doorTile, new Vector3(x*2-1, y*2, 0f), Quaternion.identity);
			}
	}


	public static void FindShortestPath(Room[,] maze, Vector2 from, Vector2 to)
	{
		int width = maze.GetLength (0);
		int height = maze.GetLength (1);
		int[,] distance = new int[width,height];
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
				maze [x, y].isOnShortestPath = false;
				distance [x, y] = -1;
			}

		// exit early if the from and to are the same
		maze[(int)from.x, (int)from.y].isOnShortestPath = true;
		if ((to - from).sqrMagnitude < float.Epsilon) {
			return;
		}

		// map out distances
		Queue<Room> queue = new Queue<Room>();
		Room currRoom = maze[(int)from.x, (int)from.y];
		distance [currRoom.x, currRoom.y] = 0;
		if (currRoom.north) queue.Enqueue (new Room(currRoom.x, currRoom.y + 1, false, true, false, false));
		if (currRoom.south) queue.Enqueue (new Room(currRoom.x, currRoom.y - 1, true, false, false, false));
		if (currRoom.east) queue.Enqueue (new Room(currRoom.x + 1, currRoom.y, false, false, false, true));
		if (currRoom.west) queue.Enqueue (new Room(currRoom.x - 1, currRoom.y, false, false, true, false));
		while (queue.Count > 0) {
			// currRoom is a x/y + connection direction
			currRoom = queue.Dequeue();
			if (currRoom.north) distance [currRoom.x, currRoom.y] = distance [currRoom.x, currRoom.y + 1] + 1;
			if (currRoom.south) distance [currRoom.x, currRoom.y] = distance [currRoom.x, currRoom.y - 1] + 1;
			if (currRoom.east) distance [currRoom.x, currRoom.y] = distance [currRoom.x + 1, currRoom.y] + 1;
			if (currRoom.west) distance [currRoom.x, currRoom.y] = distance [currRoom.x - 1, currRoom.y] + 1;

			// currRoom is x/y + all connected rooms
			currRoom = maze[currRoom.x, currRoom.y];
			if (currRoom.north && distance [currRoom.x, currRoom.y + 1] < 0) queue.Enqueue (new Room(currRoom.x, currRoom.y + 1, false, true, false, false));
			if (currRoom.south && distance [currRoom.x, currRoom.y - 1] < 0) queue.Enqueue (new Room(currRoom.x, currRoom.y - 1, true, false, false, false));
			if (currRoom.east && distance [currRoom.x + 1, currRoom.y] < 0) queue.Enqueue (new Room(currRoom.x + 1, currRoom.y, false, false, false, true));
			if (currRoom.west && distance [currRoom.x - 1, currRoom.y] < 0) queue.Enqueue (new Room(currRoom.x - 1, currRoom.y, false, false, true, false));
		}

		// move along shortest path
		currRoom = maze[(int)to.x, (int)to.y];
		while (currRoom.x != from.x || currRoom.y != from.y)
		{
			currRoom.isOnShortestPath = true;

			if (currRoom.west && distance [currRoom.x - 1, currRoom.y] == distance [currRoom.x, currRoom.y] - 1)
				currRoom = maze[currRoom.x - 1, currRoom.y];
			else if (currRoom.east && distance [currRoom.x + 1, currRoom.y] == distance [currRoom.x, currRoom.y] - 1)
				currRoom = maze[currRoom.x + 1, currRoom.y];
			else if (currRoom.south && distance [currRoom.x, currRoom.y - 1] == distance [currRoom.x, currRoom.y] - 1)
				currRoom = maze[currRoom.x, currRoom.y - 1];
			else if (currRoom.north && distance [currRoom.x, currRoom.y + 1] == distance [currRoom.x, currRoom.y] - 1)
				currRoom = maze[currRoom.x, currRoom.y + 1];
		}
	}
}
