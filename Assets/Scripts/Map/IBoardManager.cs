﻿using UnityEngine;
using System.Collections;

public abstract class IBoardManager : MonoBehaviour
{
	public abstract void SetupScene (int level);

	public abstract bool ResetPlayerPositionOnLevelChange ();
}
