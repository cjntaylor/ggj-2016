﻿using UnityEngine;
using Random = UnityEngine.Random;
using System;
using System.Collections.Generic;

public class TiledBoardLayout : IBoardManager {

	public int width = 8;
	public int height = 8;

	public MapSprites mapSprites;
	[Serializable]
	public class MapSprites
	{
		public GameObject woodFloorTile;
		public GameObject woodFloorTop_Left;
		public GameObject woodFloorTop_Center;
		public GameObject woodFloorTop_Right;
		public GameObject backWall_Left;
		public GameObject backWall_Center;
		public GameObject backWall_Right;

		public GameObject carpetBack_Left;
		public GameObject carpetBack_Center;
		public GameObject carpetBack_Right;
		public GameObject carpetMiddle_Left;
		public GameObject carpetMiddle_Center;
		public GameObject carpetMiddle_CenterAlt;
		public GameObject carpetMiddle_Right;
		public GameObject carpetFront_Left;
		public GameObject carpetFront_Center;
		public GameObject carpetFront_Right;

		public GameObject outerWall_Left;
		public GameObject outerWall_Right;
		public GameObject outerWall_BottomLeft;
		public GameObject outerWall_Bottom;
		public GameObject outerWall_BottomRight;

		public GameObject skyTile;
		public GameObject horizonTile;
		public GameObject groundTile;
		public GameObject houseFrontTile;
	}

	public ObjectSprites objectSprites;
	[Serializable]
	public class ObjectSprites
	{
		public GameObject levelEntrance;
		public GameObject levelExit;

		public GameObject[] enemyTiles;
		public GameObject[] obstacleTiles;
	}

	private Transform boardHolder = null;

	private void FloorSetup()
	{
		IRange xRange = RandomSection (1, width-2);
		IRange yRange = RandomSection (0, height-2);
		CarpetSetup (xRange, yRange);

		for (int x = -1; x < width + 1; x++) {
			for (int y = 0-1; y < height + 1; y++) {
				GameObject toInstantiate = mapSprites.woodFloorTile;

				if (xRange.inRange (x) && yRange.inRange (y))
				{
					toInstantiate = null;
				}
				else if (x == -1)
				{
					if (y == -1)
						toInstantiate = mapSprites.outerWall_BottomLeft;
					else
						toInstantiate = mapSprites.outerWall_Left;
				}
				else if (x == width)
				{
					if (y == -1)
						toInstantiate = mapSprites.outerWall_BottomRight;
					else
						toInstantiate = mapSprites.outerWall_Right;
				}
				else if (y == -1)
				{
					toInstantiate = mapSprites.outerWall_Bottom;
				}
				else if (y == height)
				{
					if (x == 0)
						toInstantiate = mapSprites.backWall_Left;
					else if (x == width - 1)
						toInstantiate = mapSprites.backWall_Right;
					else
						toInstantiate = mapSprites.backWall_Center;
				}
				else if(y == height - 1)
				{
					if (x == 0)
						toInstantiate = mapSprites.woodFloorTop_Left;
					else if (x == width - 1)
						toInstantiate = mapSprites.woodFloorTop_Right;
					else
						toInstantiate = mapSprites.woodFloorTop_Center;
				}

				if (toInstantiate != null)
				{
					GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
					instance.transform.SetParent (boardHolder);
				}
			}
		}
	}

	private void CarpetSetup(IRange xRange, IRange yRange)
	{
		for (int x = xRange.min; x <= xRange.max; x++) {
			for (int y = yRange.min; y <= yRange.max; y++) {
				GameObject toInstantiate = null;

				if (y == yRange.min) {
					if (x == xRange.min) {
						toInstantiate = mapSprites.carpetFront_Left;
					} else if (x == xRange.max) {
						toInstantiate = mapSprites.carpetFront_Right;
					} else {
						toInstantiate = mapSprites.carpetFront_Center;
					}
				} else if (y == yRange.max) {
					if (x == xRange.min) {
						toInstantiate = mapSprites.carpetBack_Left;
					} else if (x == xRange.max) {
						toInstantiate = mapSprites.carpetBack_Right;
					} else {
						toInstantiate = mapSprites.carpetBack_Center;
					}
				} else {
					if (x == xRange.min) {
						toInstantiate = mapSprites.carpetMiddle_Left;
					} else if (x == xRange.max) {
						toInstantiate = mapSprites.carpetMiddle_Right;
					} else if (Random.value < 0.3f) {
						toInstantiate = mapSprites.carpetMiddle_CenterAlt;
					} else {
						toInstantiate = mapSprites.carpetMiddle_Center;
					}
				}

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
			}
		}
	}

	private void BackgroundSetup(int level)
	{
		level--;

		// first, draw the front of the house
		for (int x = 0; x < width; x++) {
			for (int y = -level; y < 0; y++) {
				GameObject instance = Instantiate (mapSprites.houseFrontTile, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
			}
		}

		// block off the area where gameplay is
		IRange xRange = new IRange(0, width-1);
		IRange yRange = new IRange (-level, height);
		int horizon = height - level;

		// draw the sky and grass everywhere the house is not
		for (int x = -4; x < width + 4; x++) {
			for (int y = -4; y < height + 4; y++) {
				if (xRange.inRange (x) && yRange.inRange (y))
					continue;
				
				GameObject toInstantiate = mapSprites.horizonTile;
				if (y > horizon)
					toInstantiate = mapSprites.skyTile;
				if (y < horizon)
					toInstantiate = mapSprites.groundTile;

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
			}
		}
	}

	private Vector3 PlaceExit()
	{
		Vector3 exitPosition = new Vector3 ();
		// if the player is in the top/left, then the exit can only be on the bottom wall or right wall
		// let's pick the corner that's opposite the player
		if (GameManager.instance.playerInstance.transform.position.x < width / 2) {
			exitPosition.x = width - 1;
		} else {
			exitPosition.x = 0;
		}
		if (GameManager.instance.playerInstance.transform.position.y < height / 2) {
			exitPosition.y = height - 1;
		} else {
			exitPosition.y = 0;
		}
		// now, we can randomize one of the walls
		if (Random.value < 0.5f) {
			exitPosition.x = Random.Range(0, width - 1);
		} else {
			exitPosition.y = Random.Range(0, height - 1);
		}

		// place the exit
		GameObject instance = Instantiate (objectSprites.levelExit, exitPosition, Quaternion.identity) as GameObject;
		instance.transform.SetParent (boardHolder);

		// also, place the entrance at the player's location
		instance = Instantiate (objectSprites.levelEntrance, GameManager.instance.playerInstance.transform.position, Quaternion.identity) as GameObject;
		instance.transform.SetParent (boardHolder);

		return exitPosition;
	}

	//
	//
	//

	private List<Vector3> gridPositions = new List<Vector3>();
	private Vector3 RandomPosition()
	{
		int randomIndex = Random.Range(0, gridPositions.Count);
		Vector3 randomPosition = gridPositions[randomIndex];
		gridPositions.RemoveAt(randomIndex);
		return randomPosition;
	}

	private void ListGridPositions(Vector3 exitPosition)
	{
		gridPositions.Clear();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				gridPositions.Add(new Vector3(x, y, 0f));
			}
		}

		// remove places near entrance
		Vector3 entrancePosition = GameManager.instance.playerInstance.transform.position;
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				gridPositions.Remove (new Vector3(entrancePosition.x - x, entrancePosition.y + y, 0f));
//				gridPositions.Remove (new Vector3(exitPosition.x - x, exitPosition.y + y, 0f));
			}
		}

		// generate a maze
		// find the shortest path
		// remove all positions on the shortest path
		MazeGenerator.Room[,] maze = MazeGenerator.createMaze (width, height);
		MazeGenerator.FindShortestPath (maze, entrancePosition, exitPosition);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (maze [x, y].isOnShortestPath) {
					gridPositions.Remove (new Vector3(x, y, 0f));
				}
			}
		}

//		// DEBUG: fill all available tiles
//		while(gridPositions.Count > 0)
//			Instantiate(objectSprites.levelEntrance, RandomPosition(), Quaternion.identity);
	}

	void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum, int maxTile)
	{
		int objectCount = Random.Range(minimum, maximum + 1);
		if (objectCount > gridPositions.Count)
			objectCount = gridPositions.Count;
		if (maxTile < 0 || maxTile > tileArray.Length)
			maxTile = tileArray.Length;

		// make sure there is at least one of each
		for (int i = 0; i < maxTile; i++)
		{
			Vector3 randomPosition = RandomPosition();
			GameObject instance = Instantiate(tileArray[i], randomPosition, Quaternion.identity) as GameObject;
			instance.transform.SetParent(boardHolder);
			if (Random.value < 0.5)
				instance.GetComponentInChildren<SpriteRenderer>().flipX = true;
		}
		objectCount -= maxTile;

		for (; objectCount > 0; objectCount--)
		{
			Vector3 randomPosition = RandomPosition();
			int tileIndex = (int)(Random.value * Random.value * maxTile);
			GameObject instance = Instantiate(tileArray[tileIndex], randomPosition, Quaternion.identity) as GameObject;
			instance.transform.SetParent(boardHolder);
			if (Random.value < 0.5)
				instance.GetComponentInChildren<SpriteRenderer>().flipX = true;
		}
	}

	//
	//
	//

	/* generate a min/max value that is between [minAllowed, maxAllowed] and at least 3 wide (e.g. [2, 4]) */
	private struct IRange
	{
		public int min;
		public int max;
		public IRange(int min, int max) { this.min = min; this.max = max; }
		public bool inRange(int val) { return val >= min && val <= max; }
	}
	private IRange RandomSection(int minAllowed, int maxAllowed)
	{
		int min = Random.Range(minAllowed, maxAllowed+1);
		int max = Random.Range(minAllowed, maxAllowed+1);

		// swap
		if (min > max) {
			int temp = min;
			min = max;
			max = temp;
		}

		// ensure distance
		if (min + 2 > max)
			max++;
		if(max - 2 < min)
			min--;

		// ensure within range
		if (min < minAllowed) {
			min++;
			max++;
		}
		if (max > maxAllowed) {
			min--;
			max--;
		}

		return new IRange (min, max);
	}

	public override void SetupScene(int level)
	{
		// clear the old scene, create a new one
		if(boardHolder != null)
			Destroy (boardHolder.gameObject);
		boardHolder = new GameObject("Board").transform;

		FloorSetup();
		BackgroundSetup (level);
		Vector3 exit = PlaceExit();
		ListGridPositions(exit);
		LayoutObjectAtRandom(objectSprites.obstacleTiles, 3, 7, -1);
		LayoutObjectAtRandom(objectSprites.enemyTiles, level, level * 2, level);
	}

	public override bool ResetPlayerPositionOnLevelChange ()
	{
		return false;
	}
}
