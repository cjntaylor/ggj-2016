﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : IBoardManager
{
	[Serializable]
	public class Count
	{
		public int minimum;
		public int maximum;
		public Count(int min, int max) {
			minimum = min;
			maximum = max;
		}
	}

	public int columns = 8;
	public int rows = 8;
	public Count wallCount = new Count(5, 9);
	public GameObject exit;
	public GameObject[] floorTiles;
	public GameObject[] enemyTiles;
	public GameObject[] wallTiles; // TODO distinguish between inner and outer walls

	private Transform boardHolder = null;
	private List<Vector3> gridPositions = new List<Vector3>();

	void InitializeList()
	{
		gridPositions.Clear();
		for (int x = 1; x < columns - 1; x++) {
			for (int y = 1; y < rows - 1; y++) {
				if (x == 1 && y == 1)
					// it's unfair to have an enemy here
					continue;
				gridPositions.Add(new Vector3(x, y, 0f));
			}
		}
	}

	void BoardSetup() {
		if(boardHolder != null)
			Destroy (boardHolder.gameObject);

		boardHolder = new GameObject("Board").transform;
		for (int x = -1; x < columns + 1; x++) {
			for (int y = -1; y < rows + 1; y++) {
				GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
				if(x == -1 || x == columns || y == -1 || y == rows)
					toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
				
				GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

				instance.transform.SetParent(boardHolder);
			}
		}
	}

	Vector3 RandomPosition()
	{
		int randomIndex = Random.Range(0, gridPositions.Count);
		Vector3 randomPosition = gridPositions[randomIndex];
		gridPositions.RemoveAt(randomIndex);
		return randomPosition;
	}

	void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
	{
		int objectCount = Random.Range(minimum, maximum + 1);

		for (int i = 0; i < objectCount; i++)
		{
			Vector3 randomPosition = RandomPosition();
			GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
			GameObject instance = Instantiate(tileChoice, randomPosition, Quaternion.identity) as GameObject;
			instance.transform.SetParent(boardHolder);
		}
	}

	public override void SetupScene(int level)
	{
		BoardSetup();
		InitializeList();
		LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
		LayoutObjectAtRandom(enemyTiles, level, level * 2);

		GameObject exitInstance = Instantiate(exit, new Vector3(columns - 1, rows - 1, 0f), Quaternion.identity) as GameObject;
		exitInstance.transform.SetParent(boardHolder);
	}

	public override bool ResetPlayerPositionOnLevelChange ()
	{
		return true;
	}
}
