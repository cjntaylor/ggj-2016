﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class SingleAdjacent : EnemyMovement
{
	// should the enemy move every time, or skip N steps
	public int fundledoreComplaints = 0;

	// toggle for whether the enemy moves this time or not
	private int bowelProblems = 0;

	protected override Vector2 MovementDirection()
	{
		if (fundledoreComplaints > 0) {
			bowelProblems++;
			if (bowelProblems > fundledoreComplaints) {
				bowelProblems = 0;
				return new Vector2 (0, 0);
			}
		}

		if(!GameManager.instance.playerInstance.activeSelf)
			return new Vector2 (0, 0);

		Vector2 myPosition = transform.position;
		Vector2 playerLocation = GameManager.instance.playerInstance.transform.position;

		Vector2 ray = playerLocation - myPosition;
		var magnitudeDiff = Mathf.Abs(ray.x) - Mathf.Abs(ray.y);
		if (Mathf.Abs (magnitudeDiff) < 2) {
			
			// pick a random direction towards the player
			if (Random.value < 0.5) {
				return new Vector2(Mathf.Sign(ray.x), 0);
			} else {
				return new Vector2(0, Mathf.Sign(ray.y));
			}

		} else if (magnitudeDiff > 0) {
			// move along X
			return new Vector2(Mathf.Sign(ray.x), 0);
		} else {
			// move along Y
			return new Vector2(0, Mathf.Sign(ray.y));
		}
	}
}
