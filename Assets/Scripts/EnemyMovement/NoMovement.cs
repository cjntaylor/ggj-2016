﻿using UnityEngine;
using System.Collections;

public class NoMovement : EnemyMovement
{
	protected override Vector2 MovementDirection()
	{
		return new Vector2 (0, 0);
	}
}