﻿using UnityEngine;
using System.Collections;

public abstract class EnemyMovement : MovingObject
{
	public AudioClip deathSound;

	public void DoMovement()
	{
		Vector2 movement = MovementDirection();

		int horizontal = (int)movement.x;
		int vertical = (int)movement.y;
		if(horizontal != 0 || vertical != 0)
			AttemptMove<Component>(horizontal, vertical);
	}

	protected override bool OnCantMove<T>(T component)
	{
		Component other = component as Component;
		if(other.tag == "Player")
		{
			GameManager.instance.GameOver();
		}
		return false;
	}

	/* come up with a directive to go; x and y should be -1, 0, 1 */
	protected abstract Vector2 MovementDirection();

//	protected override void OnMovementFinished()
//	{
//		BoxCollider2D player = GameManager.instance.playerInstance.GetComponent<BoxCollider2D>();
//		BoxCollider2D me = GetComponent<BoxCollider2D>();
//
//		float distance = (player.transform.position - me.transform.position).sqrMagnitude;
//		float radius = (player.size - me.size).sqrMagnitude;
//
//		if(distance < radius)
//		{
//			GameManager.instance.GameOver();
//		}
//	}
}
