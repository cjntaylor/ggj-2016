﻿using UnityEngine;
using System;
using System.Collections;

public class PlayerMovement : MovingObject
{
	public float restartLevelDelay = 1f;
	public AudioClips allClips;

	// in a group so we can collapse it in the editor
	[Serializable]
	public class AudioClips
	{
		public AudioClip wallHit1;
		public AudioClip wallHit2;
	}

	void Update()
	{
		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw("Horizontal");
		vertical = (int)Input.GetAxisRaw("Vertical");

		if(horizontal != 0) vertical = 0;

		if(horizontal != 0 || vertical != 0)
			AttemptMove<Component>(horizontal, vertical);
	}

	protected override bool AttemptMove<T>(int xDir, int yDir)
	{
		bool didMove = base.AttemptMove<T>(xDir, yDir);

		if(didMove) TriggerEnemyMovement();

		return didMove;
	}

	protected override bool OnCantMove<T>(T component)
	{
		Component other = component as Component;
		if(other.tag == "Enemy")
		{
			// destroy enemy
			other.gameObject.SetActive(false);
			SoundManager.instance.PlaySingle(other.GetComponent<EnemyMovement>().deathSound);
			Destroy(other.gameObject);
			GameManager.instance.MoreScore();
			TriggerEnemyMovement();
			return true;
		} else if(other.tag == "Wall")
		{
			SoundManager.instance.RandomizeSfx(allClips.wallHit1, allClips.wallHit2);
		}

		return false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Exit")
		{
			SoundManager.instance.PlaySingle(GameManager.instance.nextLevelSound);
			Invoke("MyNextLevel", restartLevelDelay);
			enabled = false;
		}
	}

	private void MyNextLevel()
	{
		GameManager.instance.NextLevel();
		enabled = true;
	}

	protected void TriggerEnemyMovement()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		foreach(GameObject enemy in enemies)
		{
			EnemyMovement movement = enemy.GetComponent<EnemyMovement>();
			if (movement != null)
				movement.DoMovement();
		}
	}
}
