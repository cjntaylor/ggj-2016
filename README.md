Happy Sushi Friends
==============

Garrett Tjarks
Colin Taylor

A roguelike game where you run up the floors of a house trying to avoid 
getting eaten by large, sentient sushi.

Each floor is procedurally generated using maze algorithms. At least one path 
is always available thanks to a shortest path calculation.

Made with Unity.

# Building instructions

Open the src/HappySushiFriends folder in Unity, and build using the unity tools.

# Run instructions

Run the build produced by Unity or the release build provided in the release/ 
folder.

# How to Play

Get to the red portal. Every time you move, the enemies in the game move too. 
If you move into a square that an enemy moves into, you'll be killed. If you 
move into an enemy square, you'll kill the enemy, and win points.

Your goal is to get a high number point score, and get as high in the house as
you can.

# Controls

* Left Arrow - Move left
* Right Arrow - Move right
* Up Arrow - Move up
* Down Arrow - Move down